import { NavController } from '@ionic/angular';
import { Platform } from '@ionic/angular';
import { Component, OnInit } from '@angular/core';
import { Usuarios } from 'src/Models/Usuarios';
import { UsuariosPageModule } from '../usuarios/usuarios.module';
import { Mensagens } from 'src/Models/Mensagens';
import { ViewChild, AfterViewInit }  from '@angular/core';
import { 
  GoogleMaps,
  GoogleMap,
  GoogleMapsEvent,
  LatLng,
  MarkerOptions,
  Marker
  } from '@ionic-native/google-maps';

@Component({
  selector: 'app-geoloca',
  templateUrl: './geoloca.page.html',
  styleUrls: ['./geoloca.page.scss'],
})
export class GeolocaPage implements OnInit {
  nomeuser:string
  map: GoogleMap;
  
  constructor (public googleMaps:GoogleMaps, public plt:Platform, 
    public nav:NavController) {
      console.log('Construtor');
    }

  ngOnInit() {
  }

  initMap(){
    console.log('InitMap Iniciando');
    this.map = this.googleMaps.create('map');
    this.map.one(GoogleMapsEvent.MAP_READY).then ((data: any) => {
      let coordinates: LatLng = 
        new LatLng(-2.549531, -44.240750);
      let position = {
          target: coordinates,
          zoom:17
      };

      this.map.animateCamera(position);
      let markerOptions: MarkerOptions = {
        position: coordinates,
        icon: "assets/imagens/icons8-Marker-64.png",
        title: 'CEST'
      };

      const marker = this.map.addMarker(markerOptions).
      then((marker: Marker) => {
        marker.showInfoWindow();
      });
    })
  }
  ngAfterViewInit() {
    this.plt.ready().then(()=>{
      this.initMap();
    });
  }

}

