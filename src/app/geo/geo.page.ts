import { OnInit } from '@angular/core';
import {Platform, NavController, LoadingController, ToastController } from "@ionic/angular";
import { Component,ViewChild, AfterViewInit }  from '@angular/core';
import {
  GoogleMaps,
  GoogleMap,
  GoogleMapsEvent,
  LatLng,
  MarkerOptions,
  Marker,
  GoogleMapsAnimation,
  MyLocation
  } from '@ionic-native/google-maps';
import { async } from '@angular/core/testing';


@Component({
  selector: 'app-geo',
  templateUrl: './geo.page.html',
  styleUrls: ['./geo.page.scss'],
})
export class GeoPage implements OnInit {

map: GoogleMap;
loading: any;
  
  constructor (public googleMaps:GoogleMaps, public plt:Platform, 
    public nav:NavController, public loadingCtrl:LoadingController, 
    public toastCtrl: ToastController) {
      console.log('Construtor');
    }

ngOnInit() {

}

async onGPSClick(){this.map.clear();

  this.map.getMyLocation().then((location: MyLocation) => {
  this.loading.dismiss();
  console.log(JSON.stringify(location, null, 2));
})
}

async showToast(message: string) {
  let toast = await this.toastCtrl.create({
    message: message,
    duration: 2000,
    position: 'middle'
  });

  toast.present();
}

initMap(){
  console.log('InitMap Iniciando');
  this.map = this.googleMaps.create('map');
  this.map.one(GoogleMapsEvent.MAP_READY).then ((data: any) => {
    let coordinates: LatLng = 
      new LatLng(-2.549531, -44.240750);
    let position = {
        target: coordinates,
        zoom:17
    };

    this.map.animateCamera(position);
    let markerOptions: MarkerOptions = {
      position: coordinates,
      icon: "assets/imagens/icons8-Marker-64.png",
      title: 'CEST'
    };

    const marker = this.map.addMarker(markerOptions).
    then((marker: Marker) => {
      marker.showInfoWindow();
    });
  })
}

ngAfterViewInit() {
  this.plt.ready().then(()=>{
    this.initMap();
  });
}

}
