import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';

import { GoogleMaps } from '@ionic-native/google-maps';

import { AngularFireModule } from '@angular/fire';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireAuth } from '@angular/fire/auth';

var firebaseConfig = {
  apiKey: "AIzaSyCgNG_TdKlLE_MI6PcIBsKkWhLBDsjo_ng",
  authDomain: "chationic4-2fb7d.firebaseapp.com",
  databaseURL: "https://chationic4-2fb7d.firebaseio.com",
  projectId: "chationic4-2fb7d",
  storageBucket: "chationic4-2fb7d.appspot.com",
  messagingSenderId: "173854522913",
  appId: "1:173854522913:web:77e138211ef95d44c72d60",
  measurementId: "G-BDX8PFD18F"
};


@NgModule({
  declarations: [AppComponent],
  entryComponents: [],

  imports: [BrowserModule, IonicModule.forRoot(),  AngularFireAuthModule, AngularFirestoreModule, AppRoutingModule,AngularFireModule.initializeApp(firebaseConfig)],
 
  providers: [
    StatusBar,
    SplashScreen,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    GoogleMaps
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
